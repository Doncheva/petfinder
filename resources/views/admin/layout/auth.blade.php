<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link href="/css/app.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>


<nav class="navbar navbar-light white">
        <div class="container">
            <div class="navbar-header col-md-12">
                <button class="navbar-toggler togg-btn" type="button" data-toggle="collapse" data-target="#app-navbar-collapse" aria-controls="navbarSupportedContent15"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Links -->
                <ul class="navbar-nav mr-auto togg-btn">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a class="btn-link" href="{{ url('/admin/login') }}">Најави се</a></li>
                        <li><a class="btn-link" href="{{ url('/admin/register') }}">Регистрација на нов админ</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle btn-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu text-center" role="menu">
                                <li>
                                    <a  class="btn-link" href="{{ url('/admin/logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Одлогирај се
                                    </a>

                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
                <!-- Links -->
            </div>
        </div>
    </nav>

    @yield('content')

    <!-- Scripts -->
    <script src="/js/app.js"></script>


</body>
</html>
