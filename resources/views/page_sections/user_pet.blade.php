@extends('main')

@section('content')


    <div class="container-fluid">
        <div class="row pet_info">
            <div class="col-md-12 user_info">

                <!-- Trigger the modal with a button -->
                <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

                <!-- Modal -->

                <div class="" style="">


                    <!-- Modal content-->
                    <div class="p-2">
                        <h2 class="color p-1 bold"> За миленикот</h2>
                        <span class=" bold"><a href="{{route('user')}}">За мене</a></span>
                        <span class="for_pet bold">За миленикот</span>
                        <div class="line"></div>

                        <form class="container-fluid">
                            <div class="row">

                            </div>

                            <div class="row">
                                <div class="col-md-6 m-1 ">
                                    <span class="bold">Име на миленичето </span>
                                    <input type="text" placeholder="Име" id='pet_name' class="register-input">
                                </div>
                                <div class="col-md-6 m-1">
                                    <span class="bold">Тип на животно </span>
                                    <select class="register-input select" id="pet_type" name="good_with">
                                        <option disabled selected>Тип</option>
                                        @foreach($animals as $animal)
                                            <option value="{{$animal->id}}">{{$animal->type}}</option>

                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6 m-1">
                                    <span class="bold">Локација</span>
                                    <select class="register-input select" id="pet_city" name="city">
                                        <option disabled selected>Избери град</option>
                                        @foreach($cities as $city)

                                            <option value="{{$city->id}}">{{$city->name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6 m-1">
                                    <span class="bold">Возраст</span>
                                    <select class="register-input select" id="pet_age" name="">
                                        <option disabled selected>Возраст</option>
                                        @foreach($age as $value)
                                            <option value="{{$value->id}}">{{$value->age}}</option>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 m-1">
                                    <span class="bold">Добро со</span>
                                    <select class="register-input select" id="pet_good_with" name="good_with">
                                        <option disabled selected>Добро со</option>
                                        @foreach($good_with as $value)
                                            <option value="{{$value->code}}">{{$value->good_with}}</option>

                                        @endforeach
                                    </select>
                                </div>


                            </div>


                            <div class="row">

                                <div class="col-md-6 m-1">
                                    <span class="bold">Големина </span>
                                    <select class="register-input select" id="pet_size" name="size">
                                        <option disabled selected>Големина</option>
                                        @foreach($size as $value)

                                            <option value="{{$value->id}}">{{$value->size}}</option>

                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 m-1">
                                    <span class="bold"> Пол </span>
                                    <select class="register-input select" id="pet_gender" name="size">
                                        <option disabled selected>Пол</option>
                                        @foreach($gender as $value)

                                            <option value="{{$value->id}}">{{$value->gender}}</option>

                                        @endforeach
                                    </select>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-12 "
                                     style="display: flex;justify-content: center;align-items: center">
                                    <span class="bold">Додадете слики од вашето милениче</span>
                                    <div id="upload_images" class="mydropzone">


                                    </div>
                                    <div id="preview" style="display: none;">
                                        <div class="dz-preview dz-file-preview">
                                            <div class="dz-details">
                                                <div class="dz-filename"><span data-dz-name></span></div>
                                                <div class="dz-size" data-dz-size></div>
                                                <img data-dz-thumbnail/>
                                            </div>
                                            <div class="dz-progress"><span class="dz-upload"
                                                                           data-dz-uploadprogress></span></div>
                                            <div class="dz-success-mark"><span>✔</span></div>
                                            <div class="dz-error-mark"><span>✘</span></div>
                                            <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 m-1">
                                    <span class="bold"> Опис </span>
                                    <textarea rows="5" id="pet_description" placeholder="Опис"></textarea>
                                </div>

                            </div>
                            <div class="row">


                                <div class="col-md-6 m-1">

                                    <button type="button" class="btn add_pet bold" id="pets_add">Додади милениче
                                    </button>
                                </div>

                            </div>


                        </form>
                    </div>

                </div>


            </div>

        </div>


    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="gray-background">
                <div class="custom-container">
                    <h2 class="text-center card-title">Миленичиња додадени од {{Auth::user()->first_name}}</h2>

                    <div class="row cards ">
                        @foreach($pet as $value)

                            <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                                <div class="overlay-hover">
                                    <a href="" class="pet-link">
                                        <div class="card card-custom">
                                            @foreach($images as $image)
                                                @if($image->animal_id === $value->id)
                                                    <div class="card-body pading-margin">

                                                        <img class=""
                                                             src="{{asset('/storage/'.$image->image)}}">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="card-footer-border background-white"></div>
                                            <div class="card-footer text-center background-white">

                                                <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>

                                </div>
                                <div class="edit_pet">
                                    <a  class="id" data-pet_id="{{$value->id}}" ><button class="btn delete " value="" >{{$value->id}}<i class="fas fa-trash-alt"></i></button></a>
                                    {{--<button class="btn update" data-pet-id="{{$value->id}}"><i class="fas fa-edit"></i></button>--}}
                                </div>

                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection