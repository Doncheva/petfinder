<!-- Trigger the modal with a button -->
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

<!-- Modal -->
<div id="RegisterNow" class="" role="dialog">
    <div class="" style="">
        <div class="p-2">
            <button type="button" class="close" id="registration_close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
        </div>
        <div class="flex-centar m-1">
            <h2>Register</h2>
        </div>
        <!-- Modal content-->
        <div class="p-2">
            <form class="container-fluid" action="{{ route('register') }}" method="post">
                @csrf

                <div class="row">

                </div>
                <div class="row">
                    <div class="col-md-6 m-1 ">
                        <input type="text" name="first_name" " placeholder="Име" class="register-input">
                    </div>
                    <div class="col-md-6 m-1">
                        <input type="text" name="last_name" placeholder="Презиме" class="register-input">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <select class="register-input select" name="city">
                            <option disabled selected>Избери град</option>
                            @foreach($cities as $city)

                                <option value="{{$city->id}}">{{$city->name}}</option>

                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 m-1">
                        <input type="text" name="post_code" placeholder="Поштенски код" class="register-input">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <input type="email" name="email" placeholder="Email" class="register-input">
                    </div>
                    <div class="col-md-6 m-1">
                        <input type="text" name="phone_number" placeholder="Телефонски број" class="register-input">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <input type="password" name="password" id="password" placeholder="Password"
                               class="register-input">
                    </div>
                    <div class="col-md-6 m-1">
                        <input id="password-confirm" type="password" class="register-input"
                               placeholder="Confirm Password" name="password_confirmation" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <input type="checkbox" name="mail_for_promotion" class="checkbox-input">
                        <span>I would like to receive communications from Petfinder, operated by Nestlé Purina PetCare Company, including info on pet adoption and pet care, promotional materials and more. You can withdraw consent at any time. Please refer to our Privacy Policy or Contact Us page on Purina.com for more information.I would like to receive communications from Petfinder, operated by Nestlé Purina PetCare Company, including info on pet adoption and pet care, promotional materials and more. You can withdraw consent at any time. Please refer to our Privacy Policy or Contact Us page on Purina.com for more information.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <input type="checkbox" name="mail_for_news" class="checkbox-input">
                        <span>I would like to receive communications from Petfinder, operated by Nestlé Purina PetCare Company, including info on pet adoption and pet care, promotional materials and more. You can withdraw consent at any time. Please refer to our Privacy Policy or Contact Us page on Purina.com for more information.I would like to receive communications from Petfinder, operated by Nestlé Purina PetCare Company, including info on pet adoption and pet care, promotional materials and more. You can withdraw consent at any time. Please refer to our Privacy Policy or Contact Us page on Purina.com for more information.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 m-1">
                        <input type="submit" class="submit-input  bold btn btn-block">
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>


