

<div class="container-fluid">
    <div class="row" style="margin-top: 30px">
        <div class="col-xs-12 pading-margin">
            <div class="adopt-img">

            </div>

        </div>
    </div>
</div>

<!-- adopt section (laptop view)-->
<div class="container-fluid">
    <div class="row">
        <h2 class="text-center card-title hidden-xs hidden-sm">Планирате да посвоите милениче?</h2>
        <h2 class="text-center card-title-adopt background-white hidden-md hidden-lg hidden-xl">Планирате да посвоите милениче?</h2>
        <div class="hidden-xs hidden-sm col-md-4 pading-margin flex-centar">
            <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png">
            <div class=" card-adopt">
                <div class=" text-center icon-img">
                    <div class="">
                        <p class="pading-margin adopt-text">Checklist за новите вдомувачи</p>
                        <p class="adopt-description">Ви помага полесно да се адаптирате на промените.</p>
                        <a href="{{ route('checklist') }}" class="btn btn-adopt text-uppercase">Прочитај</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="hidden-xs hidden-sm col-md-4 pading-margin text-center flex-centar">
            <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png">
            <div class=" card-adopt">
                <div class=" text-center icon-img">
                    <div class="">
                        <p class="pading-margin adopt-text">Пронајди го вистинското милениче</p>
                        <p class="adopt-description">Совети кои ќе ви помогнат за да го изберете вистинското
                            милениче за вас.</p>
                        <a href="{{ route('tips') }}" class="btn btn-adopt text-uppercase">Прочитај</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="hidden-xs hidden-sm col-md-4 pading-margin text-center flex-centar">
            <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png">
            <div class=" card-adopt">
                <div class=" body-adopt text-center">
                    <div class="">
                        <p class="pading-margin adopt-text">За Brainster</p>
                        <p class="adopt-description">Сакаш и ти да научиш да програмираш вакви продукти?</p>
                        <a href="https://brainster.co/" class="btn btn-adopt text-uppercase">Прочитај</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- adopt section (phone and tablet view)-->

<div class=" hidden-md hidden-lg hidden-xl">
    <div class="carousel-adopt" data-flickity='{"wrapAround": true}'>
        <div class="carousel-cell-adopt ">
            <div class="col-md-12 pading-margin flex-centar">
                <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-18.png">
                <div class=" card-adopt">
                    <div class=" text-center icon-img">
                        <div class="">
                            <p class="pading-margin adopt-text">Checklist за новите вдомувачи</p>
                            <p class="adopt-description">Ви помага полесно да се адаптирате на промените.</p>
                        </div>
                    </div>
                </div>
                <div class="btn-adopt-xs text-center text-uppercase"><a href="{{ route('checklist') }}"><b>Прочитај</b></a></div>
            </div>
        </div>
        <div class="carousel-cell-adopt ">
            <div class="col-md-12 pading-margin text-center flex-centar">
                <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-17.png">
                <div class=" card-adopt">
                    <div class=" text-center icon-img">
                        <div class="">
                            <p class="pading-margin adopt-text">Пронајди го вистинското милениче</p>
                            <p class="adopt-description">Совети кои ќе ви помогнат за да го изберете вистинското
                                милениче.</p>
                        </div>
                    </div>
                </div>
                <div class="btn-adopt-xs text-center text-uppercase"><a href="{{ route('tips') }}"><b>Прочитај</b></a></div>
            </div>
        </div>
        <div class="carousel-cell-adopt">
            <div class="col-md-12 pading-margin text-center flex-centar">
                <img src="https://d17fnq9dkz9hgj.cloudfront.net/uploads/2017/08/PCE_Blue_Icon-8.png">
                <div class=" card-adopt">
                    <div class=" body-adopt text-center">
                        <div class="">
                            <p class="pading-margin adopt-text">За Brainster</p>
                            <p class="adopt-description">Сакаш и ти да научиш да програмираш вакви продукти?</p>
                        </div>
                    </div>
                </div>
                <div class="btn-adopt-xs text-center text-uppercase"><a href="https://brainster.co/"><b>Прочитај</b></a></div>
            </div>
        </div>
    </div>
</div>


