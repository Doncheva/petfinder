<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
    <div class="modal-dialog" id="scroll_show" role="document">
        <div class="modal-content">
            <div class="log">
                <div class="right">
                    <div class="flex-centar right-content">
                        <h3 id="Login_Modal_Secondary_Content_Header" class="color-white">Petfinder Makes Adopting
                            Easier</h3>
                        <ul class="bulletList">
                            <li class="color-white">Create and save your adopter profile.</li>
                            <li class="color-white">Save and manage your pet searches and email communications.</li>
                            <li class="color-white">Learn helpful pet care tips and receive expert advice.</li>
                            <li class="color-white">Get involved and help promote adoptable pets in your area.</li>
                        </ul>
                    </div>
                </div>
                <div class="left">
                    <div class="modal-header">
                        <button type="button" class="close" id="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>

                    </div>
                    <div class="modal-body ">
                        <h2 id="Login_Modal_Header" class="bold" tabindex="-1">Логин</h2>
                        <form class="form-group" action="{{ route('login') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" value="" placeholder="E-mail"
                                       class="form-control log_in">
                            </div>
                            <div class="form-group">


                                <input type="password" name="password" value="" placeholder="Password"
                                       class="form-control log_in">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn bold  form-control log_in_button ">Логирај се</button>

                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <div class="flex-centar bold">Нов профил?</div>
                        <button type="button" class="btn bold form-control log_in_button register_now ">Регистрација
                        </button>

                        @include('page_sections.register')
                    </div>
                </div>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

