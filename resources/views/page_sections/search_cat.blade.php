
@extends('main')

@section('content')

    <div class="container-fluid search_pat">
        <div class="row">


            <div class="col-md-2 ">
                <select class="register-input select" id="search_type">
                    <option>Сите</option>
                    @foreach($pet as $value)
                        @if($value->id == 2)
                            <option selected disabled>{{$value->type}}</option>
                        @else
                            <option value="{{$value->id}}">{{$value->type}}</option>
                        @endif
                    @endforeach
                </select>
            </div>

        </div>

    </div>
        @include('._partials.search_filter')
    <div class="container-fluid">
        <div class="row  pet_cards_row">
            <div class="gray-background">
                <div class="custom-container">
                    <div class="row cards">
                        @foreach($animals as $value)
                            <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                                <div class="overlay-hover">
                                    <a href="" class="pet-link">
                                        <div class="card card-custom">
                                            @foreach($images as $image)
                                                @if($image->animal_id === $value->id)
                                                    <div class="card-body pading-margin">
                                                        <img class=""
                                                             src="{{asset('/storage/'.$image->image)}}">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="card-footer-border background-white"></div>
                                            <div class="card-footer text-center background-white">
                                                <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="overlay overlayBottom">
                                        <a href="" class="pet-link">
                                            <div class="card card-custom">
                                                <button class="circle-for-click-open circle-for-click-open-img "></button>
                                                <div class="card-body card-body-hover circle-img text-center">
                                                    @foreach($images as $image)
                                                        @if($image->animal_id === $value->id)
                                                            <img class="img-circle responsive"
                                                                 src="{{asset('/storage/'.$image->image)}}">
                                                        @endif
                                                    @endforeach
                                                    <div class="circle-img-text color-primary">
                                                        <p class="pet-name pading-margin">
                                                            <strong>{{$value->name}}</strong></p>
                                                        <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                                        <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                                    class="dot">&#8226</span>{{$value->gender->gender}}
                                                        </p>
                                                        <p class="location pading-margin">{{$value->city->name}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 pading-margin second-one" style="margin-bottom: 50px">
                                <a href="" class="pet-link">
                                    <div class="card card-custom">
                                        <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                                        <div class="card-body circle-img text-center background-primary">
                                            @foreach($images as $image)
                                                @if($image->animal_id === $value->id)

                                                    <img class="img-circle responsive"
                                                         src="{{asset('/storage/'.$image->image)}}">
                                                @endif
                                            @endforeach
                                            <div class="circle-img-text">
                                                <p class="pet-name pading-margin color-white">
                                                    <strong>{{$value->name}}</strong></p>
                                                <br>
                                                <p class="social-links pading-margin color-white">
                                                <span class="social-links-icon social-link-selected"><i
                                                            class="fab fa-facebook-f icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-twitter icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-pinterest-p icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-envelope icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-link icon-social"></i></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection