@extends('main')

@section('content')



    <div class="container-fluid">
        <div class="row user_profile">
            <div class="col-md-12 user_info">

                <!-- Trigger the modal with a button -->
                <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

                <!-- Modal -->

                <div class="" style="">


                    <!-- Modal content-->
                    <div class="p-2">
                        <h2 class="color p-1 bold">За мене</h2>
                        <span class="for_me bold">За мене</span>
                        <span class=" bold"> <a href="{{route('pet_add')}}"> За миленикот</a></span>
                        <div class="line"></div>

                        <form class="container-fluid" method="post" action="{{route('user_update')}}">
                            {{csrf_field()}}
                            <div class="row">

                            </div>
                            <span class="bold">Вашето  имe и презиме?</span>
                            <div class="row">

                                <div class="col-md-6 m-1 ">

                                    <input type="text" placeholder="Име" name="first_name" id="first_name"
                                           value="{{$user->first_name}}"
                                           class="register-input">
                                </div>
                                <div class="col-md-6 m-1">
                                    <input type="text" placeholder="Презиме" name="last_name" id="last_name"
                                           value="{{$user->last_name}}"
                                           class="register-input">
                                </div>
                            </div>
                            <span class="bold">Вашата е-маил адреса и телефонскиот број</span>
                            <div class="row">

                                <div class="col-md-6 m-1">


                                    <input type="text" placeholder="Е-маил" disabled name="email" id="user_email"
                                           value="{{$user->email}}"
                                           class="register-input">
                                </div>
                                <div class="col-md-6 m-1">

                                    <input type="text" placeholder="Phone Number" name="phone_number"
                                           id="user_phone_number"
                                           value="{{$user->phone_number}}" class="register-input">
                                </div>
                            </div>
                            <span class="bold">Каде живееш?</span>
                            <div class="row">
                                <div class="col-md-6 m-1">

                                    <select class="register-input select" name="city" id="user_city">
                                        <option selected value="{{$user->city->id}}">{{$user->city->name}}</option>
                                        @foreach($cities as $city)
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                        @endforeach
                                    </select>
                                </div>


                            </div>

                            <div class="row">

                                <div class="col-md-6 m-1">
                                    <button type="button" class="btn add_pet bold" id="upload">Зачувај ги проментите
                                    </button>
                                </div>
                            </div>


                        </form>
                    </div>

                </div>


            </div>

        </div>


    </div>










@endsection


