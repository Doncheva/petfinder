
<!-- Carousel -->

<div class="carousel" data-flickity='{ "fullscreen": true, "lazyLoad": 2,  "wrapAround": true }'>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/720/540/?image=517" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/540/720/?image=696" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/720/540/?image=56" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/800/500/?image=1084" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/720/540/?image=1080" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/640/640/?image=1074" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/720/540/?image=1069" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/800/500/?image=1062" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image"data-flickity-lazyload="https://picsum.photos/720/540/?image=1002" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/640/640/?image=935" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/720/540/?image=855" />
    </div>
    <div class="carousel-cell">
        <img class="carousel-image" data-flickity-lazyload="https://picsum.photos/640/640/?image=824" />
    </div>
</div>
