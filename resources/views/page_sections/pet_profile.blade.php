@extends('main')

@section('content')

@include('page_sections.pet-carousel')



<!--pet description view for laptop-->
<div class="container-fluid gray-background custom-container">
    <div class="row pading-margin">
        <div class="col-md-12 hidden-sm hidden-xs slide-fixed pading-margin">
            <div class="col-md-8 hidden-sm hidden-xs pet-description-border pet-description">
                <h1 class="pet-desc-name color-primary">Спајк</h1>
                <p class="pet-desc">куче<span class="dot-desc">&#8226</span>Скопје, Македонија</p>
                <div class="hr"></div>
                <p class="pet-spec">младо<span class="dot-desc">&#8226</span>машко<span class="dot-desc">&#8226</span>среден раст<span class="dot-desc">&#8226</span>бело/црно</p>
                <div class="hr"></div>
                <h1 class="pet-desc-about color-primary">Детали за миленичето</h1>
                <p class="pet-about text-uppercase">раст:</p>
                <p class="pet-about-txt">мал</p>
                <p class="pet-about text-uppercase">за дома или во двор:</p>
                <p class="pet-about-txt">за дома</p>
                <p class="pet-about text-uppercase">здравствена состојба:</p>
                <p class="pet-about-txt">вакцинирано</p>
                <p class="pet-about text-uppercase">добро со:</p>
                <p class="pet-about-txt">деца</p>
                <div class="hr"></div>

                <h1 class="pet-desc-about color-primary">Запознај се со Спајк</h1>
                <p class="pet-about-txt">Спајк е мало кученце кое многу сака да си игра со мали деца.
                    Се радува кога е на прошетка во парк и ужива на сонце.
                    Доколку сакате некој да внесе ведрина и секојдневна насмевка кај вас и околината тогаш Спајк е вистинското милениче за вас.</p>
            </div>
                <div class="fixed-wrapper">
                    <div id="ask" class="col-md-4 hidden-sm hidden-xs side-fixed">
                        <div class="card-fixed-side">
                            <button class="btn background-white color-primary card-fixed-btn text-uppercase">Прашај за Скајп</button>
                            <div class="share-fixed">
                                <div class="half"><button class="btn btn-share-fixed btn-share-fixed-border color-white btn-block text-uppercase"><b>Сподели</b></button></div>
                                <div class="half"><button class="btn btn-share-fixed color-white btn-block text-uppercase"><b>Принтај</b></button></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>

</div>




<!--pet description view for phone and tablet-->
<div class="container-fluid gray-background">
    <div class="row">
        <div class="col-xs-12 hidden-md hidden-lg hidden-xl pet-description pading-margin">
            <h1 class="pet-desc-name color-primary text-center">Спајк</h1>
            <p class="pet-desc text-center hr-middle">куче<span class="dot-desc">&#8226</span>мобс</p></span>
            <p class="pet-spec text-center">младо<span class="dot-desc">&#8226</span>машко<span class="dot-desc">&#8226</span>среден раст<span class="dot-desc">&#8226</span>бело/црно</p>
            <div class="hr"></div>
            <p class="pet-desc text-center"><i class="fas fa-map-marker-alt"></i> Скопје, Македонија</p>
            <div class="sliding-nav">
                <ul class="slidingNav-nav">
                    <li class="slidingNav-nav-item"><a class="slidingNav-nav-item-btn">Детали</a></li>
                    <li class="slidingNav-nav-item"><a class="slidingNav-nav-item-btn">Приказна</a></li>
                    <li class="slidingNav-nav-item"><a class="slidingNav-nav-item-btn">Други миленичиња</a></li>
                    <button class="slidingNav-shareBtn">СТРЕЛКА</button>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 hidden-md hidden-lg hidden-xl pet-description description-detail">
            <h1 class="pet-desc-about color-primary">Детали за миленичето</h1>
            <div class="col-xs-6 hidden-md hidden-lg hidden-xl">
                <p class="pet-about text-uppercase">карактеристики:</p>
                <p class="pet-about-txt">Љубопитно, игриво</p>
                <p class="pet-about text-uppercase">раст:</p>
                <p class="pet-about-txt">мал</p>
                <p class="pet-about text-uppercase">за дома или во двор:</p>
                <p class="pet-about-txt">за дома</p>
                <p class="pet-about text-uppercase">здравствена состојба:</p>
                <p class="pet-about-txt">вакцинирано</p>
            </div>
            <div class="col-xs-6 hidden-md hidden-lg hidden-xl">
                <p class="pet-about text-uppercase">добро со:</p>
                <p class="pet-about-txt">деца</p>
                <p class="pet-about text-uppercase">Цена:</p>
                <p class="pet-about-txt">25000ден.</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 hidden-md hidden-lg hidden-xl pet-description description-detail">
            <h1 class="pet-desc-about color-primary">Запознај се со Спајк</h1>
            <p class="pet-about-txt">Спајк е мало кученце кое многу сака да си игра со мали деца.
                Се радува кога е на прошетка во парк и ужива на сонце.
                Доколку сакате некој да внесе ведрина и секојдневна насмевка кај вас и околината тогаш Спајк е вистинското милениче за вас.</p>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(window).on('scroll',function () {
            var top = $(window).scrollTop();
            console.log(top);
            if(top > 635){
                console.log('l');
                $('#ask').fadeOut();
            }else {
                $('#ask').fadeIn();
            }
        })
    })
</script>


@endsection