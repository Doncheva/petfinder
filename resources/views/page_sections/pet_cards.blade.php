
<!--card view for laptop-->
@if(count($pet_dog)>0)
    <div class="container-fluid">
        <div class="row  pet_cards_row">
            <div class="gray-background">
                <div class="custom-container">
                    <div class="row cards">
                        @foreach($pet_dog as $value)
                            <div class="col-md-3 hidden-xs hidden-sm pading-margin first-one" style="margin-bottom: 50px">
                                <div class="overlay-hover">
                                    <a href="" class="pet-link">
                                        <div class="card card-custom">
                                            @foreach($images_dog as $image)
                                                @if($image->animal_id === $value->id)
                                                    <div class="card-body pading-margin">
                                                        <img class=""
                                                             src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="card-footer-border background-white"></div>
                                            <div class="card-footer text-center background-white">
                                                <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="overlay overlayBottom">
                                        <a href="" class="pet-link">
                                            <div class="card card-custom">
                                                <button class="circle-for-click-open circle-for-click-open-img "></button>
                                                <div class="card-body card-body-hover circle-img text-center">
                                                    @foreach($images_dog as $image)
                                                        @if($image->animal_id === $value->id)
                                                            <img class="img-circle responsive"
                                                                 src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                                        @endif
                                                    @endforeach
                                                    <div class="circle-img-text color-primary">
                                                        <p class="pet-name pading-margin">
                                                            <strong>{{$value->name}}</strong></p>
                                                        <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                                        <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                                    class="dot">&#8226</span>{{$value->gender->gender}}
                                                        </p>
                                                        <p class="location pading-margin">{{$value->city->name}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 hidden-xs hidden-sm pading-margin second-one" style="margin-bottom: 50px">
                                <a href="" class="pet-link">
                                    <div class="card card-custom">
                                        <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                                        <div class="card-body circle-img text-center background-primary">
                                            @foreach($images_dog as $image)
                                                @if($image->animal_id === $value->id)

                                                    <img class="img-circle responsive"
                                                         src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                                @endif
                                            @endforeach
                                            <div class="circle-img-text">
                                                <p class="pet-name pading-margin color-white">
                                                    <strong>{{$value->name}}</strong></p>
                                                <br>
                                                <p class="social-links pading-margin color-white">
                                                <span class="social-links-icon social-link-selected"><i
                                                            class="fab fa-facebook-f icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-twitter icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-pinterest-p icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-envelope icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-link icon-social"></i></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                        <div class="col-md-3 hidden-xs hidden-sm pading-margin">
                            <a href="" class="pet-link">
                                <div class="card card-custom theme-card background-primary available_pets">
                                    <div class="theme-card-body text-center color-white ">
                                        <i class="fas fa-paw fa-6x"></i>
                                        <p class="theme-card-p">Уште {{$count}} достапни миленичиња на Petfinder</p>
                                    </div>

                                    <div class="theme-card-footer card-footer text-center color-white">
                                        <p class="text-uppercase"><strong>запознајте ги</strong></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(count($pet_cat)>0)

    !!! pet cards (cats)!!!
    <div class="container-fluid">
        <div class="row  pet_cards_row">
            <div class="gray-background">
                <div class="custom-container">
                    <div class="row cards">
                        @foreach($pet_cat as $value)
                            <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                                <div class="overlay-hover">
                                    <a href="" class="pet-link">
                                        <div class="card card-custom">
                                            @foreach($images_cat as $image)
                                                @if($image->animal_id === $value->id)
                                                    <div class="card-body pading-margin">
                                                        <img class=""
                                                             src="{{asset('/storage/'.$image->image)}}">
                                                    </div>
                                                @endif
                                            @endforeach
                                            <div class="card-footer-border background-white"></div>
                                            <div class="card-footer text-center background-white">
                                                <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="overlay overlayBottom">
                                        <a href="" class="pet-link">
                                            <div class="card card-custom">
                                                <button class="circle-for-click-open circle-for-click-open-img "></button>
                                                <div class="card-body card-body-hover circle-img text-center">
                                                    @foreach($images_cat as $image)
                                                        @if($image->animal_id === $value->id)
                                                            <img class="img-circle responsive"
                                                                 src="{{asset('/storage/'.$image->image)}}">
                                                        @endif
                                                    @endforeach
                                                    <div class="circle-img-text color-primary">
                                                        <p class="pet-name pading-margin">
                                                            <strong>{{$value->name}}</strong></p>
                                                        <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                                        <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                                    class="dot">&#8226</span>{{$value->gender->gender}}
                                                        </p>
                                                        <p class="location pading-margin">{{$value->city->name}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-3 pading-margin second-one" style="margin-bottom: 50px">
                                <a href="" class="pet-link">
                                    <div class="card card-custom">
                                        <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                                        <div class="card-body circle-img text-center background-primary">
                                            @foreach($images_cat as $image)
                                                @if($image->animal_id === $value->id)

                                                    <img class="img-circle responsive"
                                                         src="{{asset('/storage/'.$image->image)}}">
                                                @endif
                                            @endforeach
                                            <div class="circle-img-text">
                                                <p class="pet-name pading-margin color-white">
                                                    <strong>{{$value->name}}</strong></p>
                                                <br>
                                                <p class="social-links pading-margin color-white">
                                            <span class="social-links-icon social-link-selected"><i
                                                        class="fab fa-facebook-f icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-twitter icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fab fa-pinterest-p icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-envelope icon-social"></i></span>
                                                    <span class="social-links-icon"><i
                                                                class="fas fa-link icon-social"></i></span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                        <div class="col-md-3 pading-margin">
                            <a href="" class="pet-link">
                                <div class="card card-custom theme-card background-primary available_pets">
                                    <div class="theme-card-body text-center color-white ">
                                        <i class="fas fa-paw fa-6x"></i>
                                        <p class="theme-card-p">Уште {{$count}} достапни миленичиња на Petfinder</p>
                                    </div>

                                    <div class="theme-card-footer card-footer text-center color-white">
                                        <p class="text-uppercase"><strong>запознајте ги</strong></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Carousel (card view for phone and tablet)-->
    <div class=" hidden-md hidden-lg hidden-xl">
    <div class="carousel-card" data-flickity='{"wrapAround": true}'>
        <div class="carousel-cell-card ">
            @foreach($pet_dog as $value)
                <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                    <div class="overlay-hover">
                        <a href="" class="pet-link">
                            <div class="card card-custom">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)
                                        <div class="card-body pading-margin">
                                            <img class=""
                                                 src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                        </div>
                                    @endif
                                @endforeach
                                <div class="card-footer-border background-white"></div>
                                <div class="card-footer text-center background-white">
                                    <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                    </h3>
                                </div>
                            </div>
                        </a>
                        <div class="overlay overlayBottom">
                            <a href="" class="pet-link">
                                <div class="card card-custom">
                                    <button class="circle-for-click-open circle-for-click-open-img "></button>
                                    <div class="card-body card-body-hover circle-img text-center">
                                        @foreach($images_dog as $image)
                                            @if($image->animal_id === $value->id)
                                                <img class="img-circle responsive"
                                                     src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                            @endif
                                        @endforeach
                                        <div class="circle-img-text color-primary">
                                            <p class="pet-name pading-margin">
                                                <strong>{{$value->name}}</strong></p>
                                            <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                            <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                        class="dot">&#8226</span>{{$value->gender->gender}}
                                            </p>
                                            <p class="location pading-margin">{{$value->city->name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 pading-margin second-one" style="margin-bottom: 50px">
                    <a href="" class="pet-link">
                        <div class="card card-custom">
                            <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                            <div class="card-body circle-img text-center background-primary">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)

                                        <img class="img-circle responsive"
                                             src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                    @endif
                                @endforeach
                                <div class="circle-img-text">
                                    <p class="pet-name pading-margin color-white">
                                        <strong>{{$value->name}}</strong></p>
                                    <br>
                                    <p class="social-links pading-margin color-white">
                                                <span class="social-links-icon social-link-selected"><i
                                                            class="fab fa-facebook-f icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-twitter icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-pinterest-p icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-envelope icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-link icon-social"></i></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="carousel-cell-card">
            @foreach($pet_dog as $value)
                <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                    <div class="overlay-hover">
                        <a href="" class="pet-link">
                            <div class="card card-custom">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)
                                        <div class="card-body pading-margin">
                                            <img class=""
                                                 src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                        </div>
                                    @endif
                                @endforeach
                                <div class="card-footer-border background-white"></div>
                                <div class="card-footer text-center background-white">
                                    <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                    </h3>
                                </div>
                            </div>
                        </a>
                        <div class="overlay overlayBottom">
                            <a href="" class="pet-link">
                                <div class="card card-custom">
                                    <button class="circle-for-click-open circle-for-click-open-img "></button>
                                    <div class="card-body card-body-hover circle-img text-center">
                                        @foreach($images_dog as $image)
                                            @if($image->animal_id === $value->id)
                                                <img class="img-circle responsive"
                                                     src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                            @endif
                                        @endforeach
                                        <div class="circle-img-text color-primary">
                                            <p class="pet-name pading-margin">
                                                <strong>{{$value->name}}</strong></p>
                                            <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                            <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                        class="dot">&#8226</span>{{$value->gender->gender}}
                                            </p>
                                            <p class="location pading-margin">{{$value->city->name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 pading-margin second-one" style="margin-bottom: 50px">
                    <a href="" class="pet-link">
                        <div class="card card-custom">
                            <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                            <div class="card-body circle-img text-center background-primary">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)

                                        <img class="img-circle responsive"
                                             src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                    @endif
                                @endforeach
                                <div class="circle-img-text">
                                    <p class="pet-name pading-margin color-white">
                                        <strong>{{$value->name}}</strong></p>
                                    <br>
                                    <p class="social-links pading-margin color-white">
                                                <span class="social-links-icon social-link-selected"><i
                                                            class="fab fa-facebook-f icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-twitter icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-pinterest-p icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-envelope icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-link icon-social"></i></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="carousel-cell-card">
            @foreach($pet_dog as $value)
                <div class="col-md-3 pading-margin first-one" style="margin-bottom: 50px">
                    <div class="overlay-hover">
                        <a href="" class="pet-link">
                            <div class="card card-custom">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)
                                        <div class="card-body pading-margin">
                                            <img class=""
                                                 src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                        </div>
                                    @endif
                                @endforeach
                                <div class="card-footer-border background-white"></div>
                                <div class="card-footer text-center background-white">
                                    <h3 class="pet-name color-primary"><strong>{{$value->name}}</strong>
                                    </h3>
                                </div>
                            </div>
                        </a>
                        <div class="overlay overlayBottom">
                            <a href="" class="pet-link">
                                <div class="card card-custom">
                                    <button class="circle-for-click-open circle-for-click-open-img "></button>
                                    <div class="card-body card-body-hover circle-img text-center">
                                        @foreach($images_dog as $image)
                                            @if($image->animal_id === $value->id)
                                                <img class="img-circle responsive"
                                                     src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                            @endif
                                        @endforeach
                                        <div class="circle-img-text color-primary">
                                            <p class="pet-name pading-margin">
                                                <strong>{{$value->name}}</strong></p>
                                            <p class="pet-type pading-margin">{{$value->size->size}}</p>
                                            <p class="years-gender pading-margin">{{$value->age->age}} <span
                                                        class="dot">&#8226</span>{{$value->gender->gender}}
                                            </p>
                                            <p class="location pading-margin">{{$value->city->name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="col-md-3 pading-margin second-one" style="margin-bottom: 50px">
                    <a href="" class="pet-link">
                        <div class="card card-custom">
                            <button class="circle-for-click-close"><i class="fas fa-times"></i></button>

                            <div class="card-body circle-img text-center background-primary">
                                @foreach($images_dog as $image)
                                    @if($image->animal_id === $value->id)

                                        <img class="img-circle responsive"
                                             src="https://www.telegraph.co.uk/content/dam/news/2018/01/25/TELEMMGLPICT000144075850_trans_NvBQzQNjv4BqtBZSUvGe3rsJxi0SXUlx8GyKXiPqmHbXbjFyMKQyNKk.jpeg?imwidth=450">
                                    @endif
                                @endforeach
                                <div class="circle-img-text">
                                    <p class="pet-name pading-margin color-white">
                                        <strong>{{$value->name}}</strong></p>
                                    <br>
                                    <p class="social-links pading-margin color-white">
                                                <span class="social-links-icon social-link-selected"><i
                                                            class="fab fa-facebook-f icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-twitter icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fab fa-pinterest-p icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-envelope icon-social"></i></span>
                                        <span class="social-links-icon"><i
                                                    class="fas fa-link icon-social"></i></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <div class="carousel-cell-card">
            <div class="col-md-3 pading-margin">
                <a href="" class="pet-link">
                    <div class="card card-custom theme-card background-primary available_pets">
                        <div class="theme-card-body text-center color-white ">
                            <i class="fas fa-paw fa-6x"></i>
                            <p class="theme-card-p">Уште {{$count}} достапни миленичиња на Petfinder</p>
                        </div>

                        <div class="theme-card-footer card-footer text-center color-white">
                            <p class="text-uppercase"><strong>запознајте ги</strong></p>
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>
    </div>
