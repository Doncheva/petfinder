<div class="container-fluid">
    <div class="row" style="margin-left: 20px">
        <div class="col-md-2 m-1">
            <span class="bold">Град</span>
            <div class="register-input custom-select" id="pet_city" name="city">
                <div id="city-dropdown">Избери град <i style="margin-left: 10px"  class="fas fa-angle-down " ></i></div>
                <div class="custom-select-parent">
                    @foreach($cities as $city)
                        <div class="custom-select-option"     value="{{$city->id}}">{{$city->name}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-2 m-1">
            <span class="bold">Возраст</span>
            <div class="register-input custom-select" id="pet_age" name="">
                <div >Возраст <i style="margin-left: 10px"  class="fas fa-angle-down"></i></div>
                <div class="custom-select-parent">
                    @foreach($age as $value)
                        <div class="custom-select-option" value="{{$value->id}}">{{$value->age}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-2 m-1">
            <span class="bold">Големина </span>
            <div class="register-input custom-select" id="pet_size" name="size">
                <div >Големина  <i style="margin-left: 10px"  class="fas fa-angle-down"></i></div>
                <div class="custom-select-parent">
                    @foreach($size as $value)
                        <div class="custom-select-option" value="{{$value->id}}">{{$value->size}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-2 m-1">
            <span class="bold"> Пол </span>
            <div class="register-input custom-select" id="pet_gender" name="size">
                <div >Пол <i style="margin-left: 10px"  class="fas fa-angle-down"></i></div>
                <div class="custom-select-parent">
                    @foreach($gender as $value)
                        <div class="custom-select-option" value="{{$value->id}}">{{$value->gender}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-2 m-1">
            <span class="bold">Добро со</span>
            <div class="register-input custom-select" id="pet_good_with" name="good_with">
                <div >Добро со <i style="margin-left: 10px"  class="fas fa-angle-down"></i></div>
                <div class="custom-select-parent">
                    @foreach($good_with as $value)
                        <div class="custom-select-option" value="{{$value->code}}">{{$value->good_with}}</div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-2 " style="margin-top: 33px">
            <button class="btn btn-danger btn-lg">Пребарувај</button>
        </div>

    </div>
</div>