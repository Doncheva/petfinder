{{--footer section--}}
<div class="container-fluid pading-margin footer">
    <div class="top-footer-section">
        <img class="left-top-footer" src="https://blog.brainster.co/wp-content/themes/pogon/images/logo-footer.png">
        <div class="row">
            <div class="col-md-6 footer-text-position">
                <h2 class="footer-text footer-color">Изработено од <span
                            class="different-footer-text-red">учесниците</span> на Академијата за Програмирање и
                    Академијата за Маркетинг на Brainster</h2>
            </div>
            <div class="col-md-6 footer-text-position">
                <h5><a href="https://brainster.co/">brainster.co</a></h5>
                <h5><a href="http://www.brainster.io/">brainster.io/business</a></h5>
                <h5><a href="http://codepreneurs.co/">codepreneurs.co</a></h5>
                <h5 class="footer-color">contact@brainster.co</h5>
                <h5 class="footer-color">+38970384728</h5>
                <h5 class="footer-color">ул. Златко Шнајдер 4а-3, 1000 Скопје</h5>
                <h5 class="footer-color">
                    <a href="https://www.facebook.com/brainster.co/" class="footer-icon"><i
                                class="fab fa-facebook-square"></i></a>
                    <a href="https://twitter.com/brainsterco?lang=en" class="footer-icon"><i
                                class="fab fa-twitter-square"></i></a>
                    <a href="https://www.instagram.com/brainsterco/?hl=en" class="footer-icon"><i
                                class="fab fa-instagram"></i></a>
                    <a href="https://www.linkedin.com/company/brainster-co/" class="footer-icon"><i
                                class="fab fa-linkedin-in footer-icon"></i></a>
                </h5>
            </div>
        </div>
    </div>
</div>
