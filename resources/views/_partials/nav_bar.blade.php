<nav class="navbar navbar-default ">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand font-gogle flex-centar" href="{{route('home_page')}}">Petfinder</a>
        </div>
        <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">


                <li class="nav-lista ">
                    <button type="button " class="btn  resources  bold" data-toggle="collapse" id="up-down"
                            data-target="#clickResouces" name="button"> RESOUCES <i id="toggle"
                                                                                    class="fas fa-angle-down"></i>
                    </button>
                </li>


            </ul>
            <ul class="nav navbar-nav navbar-right ">


                <li class="nav-lista"><a href="#"><i class="fas fa-search "></i></a></li>

                @auth
                    <li class="nav-lista bold"><a href="{{route('pet_add')}}" id="add_pet">Додади милениче</a></li>
                    <li class="nav-item dropdown logOut">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->first_name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right log_out" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{ route('user') }}">
                                <p>Account info</p>
                            </a>

                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <p>{{ __('Одјави се') }}</p>
                            </a>


                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-lista bold"><a id="add_pet">Додади милениче</a></li>
                    <li class="nav-lista ">
                        <button type="button" class="btn login_register bold" name="button" data-toggle="modal"
                                data-target="#myModal"><i class="fas fa-user"></i> Логин\Регистрација
                        </button>
                    </li>
                    @include('page_sections.login')

                @endauth
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container-fluid collapse affter-navbar " id="clickResouces">
    <div class="row ">
        <div class="col-md-4 m-1 flex-centar ">
            <h4 class="color-white  bold">Организација за заштита на житвотните</h4>
        </div>
        <div class="col-md-4 m-1 flex-centar">
            <h4 class="color-white bold">Други продукти на Brainster </h4>
        </div>
        <div class="col-md-4 m-1 flex-centar">
            <h4 class="color-white bold">Изработено од студентите на Brainster </h4>
        </div>

    </div>

</div>