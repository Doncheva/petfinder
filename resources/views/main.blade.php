<!doctype html>
<html lang="en">
@include('_partials.head')


<body>
@include('_partials.nav_bar')

@yield('content')

@include('_partials.footer')

@include('_partials.script')
</body>
</html>