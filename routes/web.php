<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@view_home_page')->name('home_page');
Route::get('/user', 'UserController@user_pprofile')->name('user');
Route::get('/user/pet', 'UserController@pet_add')->name('pet_add');
Route::get('/search/pet', 'PetController@search')->name('search_pet');
Route::post('/user/save', 'UserController@saveuser')->name('save_user_registration');
Route::post('/upload/images', 'UserController@upload_images')->name('upload_images');
Route::post('/pet/save', 'UserController@pet_save')->name('pet_save');
Route::post('/user/update', 'UserController@user_update')->name('user_update');
Route::post('/search/pet/dog','PetController@search_dog');

Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pet/profile', 'UserController@pet_profile')->name('pet_profile');
Route::get('/pet/checklist', 'UserController@checklist')->name('checklist');
Route::get('/pet/tips', 'UserController@tips')->name('tips');

Route::get('/search/dog', 'PetController@search_dog_view')->name('search_dog');
Route::get('/search/cat', 'PetController@search_cat_view')->name('search_cat');
Route::post('/delete/pet','UserController@pet_delete')->name('delete_pet');

Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin_login');
  Route::post('/login', 'AdminAuth\LoginController@login');
  Route::post('/logout', 'AdminAuth\LoginController@logout')->name('admin_logout');

  Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('admin_register');
  Route::post('/register', 'AdminAuth\RegisterController@register');

  Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.request');
  Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin_password.email');
  Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.reset');
  Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});
