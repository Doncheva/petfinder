<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Age extends Model
{
    protected  $table = 'age';
    public function animal(){
        return $this->hasMany(Info_Animal::class);
    }
}
