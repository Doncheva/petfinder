<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected  $table = 'gender';
    public function animal(){
        return $this->hasMany(Info_Animal::class);
    }
}
