<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload_Image extends Model
{
    protected  $table = 'upload_images';
    public function info_animal(){
        $this->belongsTo(Info_Animal::class,'id','animal_id');
    }
}
