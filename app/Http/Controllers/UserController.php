<?php

namespace App\Http\Controllers;

use App\Age;
use App\Animal;
use App\City;
use App\Gender;
use App\Good_With;
use App\Http\Requests\UserUpdateValidation;
use App\Info_Animal;
use App\Size;
use App\Upload_Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function user_pprofile()
    {

        $user = User::with('city')->where('id', Auth::id())->first();

        $cities = City::where('id', '!=', $user->city->id)->get();
        return view('page_sections.user_profile', compact('user', 'cities'));
    }

    public function pet_profile()
    {
        return view('page_sections.pet_profile');
    }


    public function checklist()
    {
        return view('page_sections.checklist');
    }

    public function tips()
    {
        return view('page_sections.tips');
    }


    public function pet_add()
    {
        $age = Age::all();
        $animals = Animal::all();
        $cities = City::all();
        $gender = Gender::all();
        $good_with = Good_With::all();
        $size = Size::all();

        $pet = Info_Animal::with('gender', 'city', 'size', 'age')->where('user_id', Auth::id())->get();
        $images = [];

        foreach ($pet as $value) {

            foreach ($value->image as $image) {

                array_push($images, $image);
                break;

            }

        }

        return view('page_sections.user_pet', compact('age', 'animals', 'cities', 'gender', 'good_with', 'size', 'pet', 'images', 'user_name'));
    }

    public function upload_images(Request $request)
    {
        $images = $request->file;
        $pet_id = $request->pet;


        foreach ($images as $value) {
            $picture = new Upload_Image;
            $picture->animal_id = $pet_id;
            $picture->image = $value->store('images');
            $picture->save();

        }

        return response()->json(['nesto']);
    }

    public function pet_save(Request $request)
    {
        $name = $request->name;
        $type = $request->type;
        $city = $request->city;
        $age = $request->age;
        $size = $request->size;
        $gender = $request->gender;
        $good_with = $request->good_with;
        $description = $request->description;
        $user = Auth::id();
        $pet = new Info_Animal;
        $pet->name = $name;
        $pet->description = $description;
        $pet->gender_id = $gender;
        $pet->age_id = $age;
        $pet->animal_id = $type;
        $pet->city_id = $city;
        $pet->user_id = $user;
        $pet->good_with = json_encode($good_with);
        $pet->size_id = $size;
        $pet->save();

        return response()->json(['pet_id' => $pet->id]);


    }

    public function user_update(UserUpdateValidation $request)
    {
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $city = $request->city;
        $phone_number = $request->phone_number;
        $user = User::find(Auth::id());
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->city_id = $city;
        $user->phone_number = $phone_number;
        $user->save();


        return redirect()->route('user');


    }
    public function pet_delete(Request $request){
        Info_Animal::destroy($request->pet_id);
        return response()->json();
    }
}
