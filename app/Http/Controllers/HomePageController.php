<?php

namespace App\Http\Controllers;

use App\City;
use App\Info_Animal;
use Illuminate\Http\Request;

class HomePageController extends Controller
{


    public function view_home_page()
    {

        $cities = City::all();

        $pet_dog = Info_Animal::with('image', 'gender', 'size', 'city', 'age')->where('animal_id', 1)->get();
        $pet_cat = Info_Animal::with('image', 'gender', 'size', 'city', 'age')->where('animal_id', 2)->get();


        if (count($pet_dog) >= 3){
            $pet_dog = $pet_dog->random(3);
        }

        if (count($pet_cat) >= 3){
            $pet_cat = $pet_cat->random(3);
        }



        $images_dog = [];
        $images_cat = [];
        foreach ($pet_dog as $value) {

            foreach ($value->image as $image) {

                array_push($images_dog, $image);
                break;

            }

        }
        foreach ($pet_cat as $value) {

            foreach ($value->image as $image) {

                array_push($images_cat, $image);
                break;

            }

        }
        $count_pet_dog = count($pet_dog);
        $count_pet_cat = count($pet_cat);
        $count = Info_Animal::all()->count();
        $count = $count - $count_pet_cat - $count_pet_dog;

        return view('index', compact('cities', 'pet_dog', 'images_dog', 'images_cat', 'pet_cat', 'count'));
    }
}
