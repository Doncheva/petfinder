<?php

namespace App\Http\Controllers;

use App\Age;
use App\Animal;
use App\City;
use App\Gender;
use App\Good_With;
use App\Info_Animal;
use App\Size;
use Illuminate\Http\Request;

class PetController extends Controller
{


    public function search()
    {
        $age = Age::all();
        $pet = Animal::all();
        $cities = City::all();
        $gender = Gender::all();
        $good_with = Good_With::all();
        $size = Size::all();
        $animals = Info_Animal::with('gender', 'age', 'size', 'city')->get();
        if (count($animals) >= 8) {
            $animals->random(8);
        }

        $images = [];
        foreach ($animals as $value) {

            foreach ($value->image as $image) {

                array_push($images, $image);
                break;

            }

        }

        return view('page_sections.search', compact('age', 'pet', 'cities', 'gender', 'good_with', 'size', 'images', 'animals'));
    }

    public function search_dog_view()
    {
        $age = Age::all();
        $pet = Animal::all();
        $cities = City::all();
        $gender = Gender::all();
        $good_with = Good_With::all();
        $size = Size::all();
        $animals = Info_Animal::with('gender', 'age', 'size', 'city')->where('animal_id', 1)->get();
        if (count($animals) >= 8) {
            $animals->random(8);
        }

        $images = [];
        foreach ($animals as $value) {

            foreach ($value->image as $image) {

                array_push($images, $image);
                break;

            }

        }


        return view('page_sections.search_dog', compact('age', 'pet', 'cities', 'gender', 'good_with', 'size', 'images', 'animals'));

    }

    public function search_cat_view()
    {
        $age = Age::all();
        $pet = Animal::all();
        $cities = City::all();
        $gender = Gender::all();
        $good_with = Good_With::all();
        $size = Size::all();
        $animals = Info_Animal::with('gender', 'age', 'size', 'city')->where('animal_id', 2)->get();
        if (count($animals) >= 8) {
            $animals->random(8);
        }
        $images = [];
        foreach ($animals as $value) {

            foreach ($value->image as $image) {

                array_push($images, $image);
                break;

            }

        }


        return view('page_sections.search_cat', compact('age', 'pet', 'cities', 'gender', 'good_with', 'size', 'images', 'animals'));

    }

    public function search_dog(Request $request)
    {
        $animals = Info_Animal::with('gender', 'age', 'size', 'city');
        if (count($request->city) > 0) {
            $animals->whereIn('city_id', $request->city);

        }
        if (count($request->size) > 0) {
            $animals->whereIn('size_id', $request->size);

        }
        if (count($request->gender) > 0) {
            $animals->whereIn('gender_id', $request->gender);

        }
        if (count($request->age) > 0) {
            $animals->whereIn('age_id', $request->age);

        }
        $all_animals = Info_Animal::all();
        $array = [];

        if (count($request->good_with) > 0) {

            foreach ($all_animals as $value) {

                $good_with = $value->good_with;
                if (array_intersect(json_decode($value->good_with), $request->good_with)) {
                    array_push($array, $good_with);

                }
            }
            $animals->whereIn('good_with', $array);


        }

        $animals = $animals->get();
        $images = [];
        foreach ($animals as $value) {

            foreach ($value->image as $image) {

                array_push($images, $image);
                break;

            }

        }

        return view('page_sections.search_dog', compact('animals', 'images'));
    }

}





