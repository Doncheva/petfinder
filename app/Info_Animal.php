<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Info_Animal extends Model
{
    protected  $table ="info_animals";

    public function age(){

        return $this->belongsTo(Age::class);

    }
    public function  animal(){
        return  $this->belongsTo(Animal::class);
    }
    public function city(){
        return  $this->belongsTo(City::class);
    }
    public function gender(){
        return  $this->belongsTo(Gender::class);

    }
    public function size(){
      return  $this->belongsTo(Size::class);
    }
    public function user(){
       return $this->belongsTo(User::class);
    }
    public function image(){
        return $this->hasMany(Upload_Image::class,'animal_id','id');
    }
}
