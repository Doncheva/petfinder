<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function users(){
      return  $this->hasMany(User::class);
    }
    public function animal(){
        return $this->hasMany(Info_Animal::class);
    }
}
