<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    public function animals(){
        return $this->hasMany(Info_Animal::class);
    }
}
