<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AgeTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(AnimalsTableSeeder::class);
        $this->call(GenderTableSeeder::class);
        $this->call(GoodWhithTableSeeder::class);
        $this->call(SizeTableSeeder::class);
    }
}
