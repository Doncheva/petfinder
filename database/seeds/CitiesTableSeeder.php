<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Арачиново', 'Берово', 'Битола', 'Богданци', 'Боговиње', 'Босилово', 'Брвеница', 'Валандово', 'Василево',
            'Вевчани', 'Велес', 'Виница', 'Вранештица', 'Врапчиште', 'Гевгелија', 'Гостивар', 'Градско', 'Дебар',' Дебарца', 'Делчево',' Демир Капија',
            'Демир Хисар', 'Дојран', 'Долнени', 'Другово', 'Желино', 'Зајас', 'Зелениково', 'Зрновци', 'Илинден', 'Јегуновце','Кавадарци','Карбинци','Кичево','Конче','Кочани','Кратово','Крива Паланка','Кривогаштани','Крушево', 'Куманово', 'Липково',
            'Лозово', 'Маврово и Ростуше', 'Македонски Брод', 'Македонска Каменица', 'Могила', 'Неготино', 'Новаци', 'Ново Село' , 'Осломеј',
            'Охрид', 'Петровец', 'Пехчево', 'Пласница', 'Прилеп', 'Пробиштип', 'Радовиш', 'Ресен', 'Росоман', 'Старо Нагоричане', 'Свети Николе', 'Сопиште',
            'Струга', 'Струмица', 'Студеничани', 'Теарце', 'Тетово', 'Центар Жупа', 'Чашка', 'Чешиново и Облешево', 'Чучер Сандево', 'Штип', 'Аеродром', 'Бутел',
            'Гази Баба', 'Ѓорче Петров', 'Карпош', 'Кисела Вода', 'Сара', 'Центар', 'Чаир', 'Шуто Оризари', 'Град Скопје'];
        foreach ($array as $value){

            $name = new \App\City;
            $name->name = $value;
            $name->save();

        }
    }
}
