<?php

use Illuminate\Database\Seeder;

class AgeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Младо','Возрасно','Старо'];
        foreach ($array as $value){

            $age = new \App\Age;
            $age->age = $value;
            $age->save();

        }
    }
}
