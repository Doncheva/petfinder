<?php

use Illuminate\Database\Seeder;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Мал раст','Среден Раст','Голем Раст'];
        foreach ($array as $value){

            $size = new \App\Size();
            $size->size = $value;
            $size->save();

        }

    }
}
