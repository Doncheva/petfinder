<?php

use Illuminate\Database\Seeder;

class GoodWhithTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Мачки','Кучиња','Деца'];
        $code = ['C','D','K'];
        foreach ($array as $key => $value){

            $type = new \App\Good_With;
            $type->good_with = $value;
            $type->code = $code[$key];
            $type->save();


        }
    }
}
