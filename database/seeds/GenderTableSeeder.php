<?php

use Illuminate\Database\Seeder;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = ['Машки','Женски'];
        foreach ($array as $value){

            $gender = new \App\Gender;
            $gender->gender = $value;
            $gender->save();

        }
    }
}
