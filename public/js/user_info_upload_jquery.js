$(document).ready(function () {
    $("#upload").on('click', function () {
        var first = $("#first_name").val();
        var last = $("#last_name").val();
        var email = $("#user_email").val();
        var city = $("#user_city").val();
        var phone_number = $("#user_phone_number").val();

        if (first == "") {
            $("#first_name").css("border", "1px solid red");
            $("#first_name").focus();
            return;
        }
        if (last == "") {
            $("#last_name").css("border", "1px solid red");
            $("#last_name").focus();
            return;
        }
        if (email == "") {
            $("#user_email").css("border", "1px solid red");
            $("#user_email").focus();
            return;
        }
        if (city == "") {
            $("#user_city").css("border", "1px solid red");
            $("#user_city").focus();
            return;
        }
        if (phone_number == "") {
            $("#user_phone_number").css("border", "1px solid red");
            $("#user_phone_number").focus();
            return;
        }
        $('form').submit();


    })
})