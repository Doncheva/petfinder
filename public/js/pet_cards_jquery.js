$(document).ready(function () {
    $('.circle-for-click-open').click(function (e) {
        e.preventDefault();

        $('.second-one').hide();
        $('.first-one').show();
        $(this).parents('.first-one').hide();
        $(this).parents('.first-one').next('.second-one').show();
    });

    $('.circle-for-click-close').click(function (e) {
        e.preventDefault();
        // $(this).find('.first-one').hide();
        // $(this).find('.second-one').show();
        $('.first-one').show();
        $('.second-one').hide();
    });

    $('.circle-for-click-close').click(function () {
        $(this).toggleClass('shown'); // toggle hiding and showing
    });



    //carousel:
    $('#myCarousel').carousel({
        interval: 40000
    });

    $('.carousel .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length>0) {

            next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');

        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));

        }
    });
    


});