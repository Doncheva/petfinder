$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#pets_add').on('click', function () {
        var name = $('#pet_name').val();
        var type = $('#pet_type').val();
        var city = $('#pet_city').val();
        var age = $('#pet_age').val();
        var good_with = $('#pet_good_with').val();
        var size = $('#pet_size').val();
        var gender = $('#pet_gender').val();
        var description = $('#pet_description').val();

        $.ajax({
            url: `/pet/save`,
            method: `POST`,
            data:
                {
                    "name": name,
                    "type": type,
                    "city": city,
                    "age": age,
                    "good_with": [good_with],
                    "size": size,
                    "gender": gender,

                    "description": description


                },


        }).done(function (data) {
            var id = data.pet_id;
            $('#pets_add').attr('data-pet', id).attr('disable', true);

            var myDropzone = Dropzone.forElement("#upload_images");
            myDropzone.processQueue();

        })

    })
    $("#upload_images").dropzone({
        url: '/upload/images',
        paramName: 'file',
        autoProcessQueue: false,
        clickable: true,
        enqueueForUpload: true,
        maxFilesize: 50,
        maxFiles: 10,

        uploadMultiple: true,
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
        maxfilesreached: function () {

            alert('max files reached');

        },
        maxfilesexceeded: function () {
            alert('max files reached 2');

            this.removeAllFiles();
            this.reset();

        },
        sending: function (file, xhr, formData) {
            formData.append('pet', $('#pets_add').attr('data-pet'));

        },
        completemultiple:function () {

            window.location.reload();
        }

    });

})