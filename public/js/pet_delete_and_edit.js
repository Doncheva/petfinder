$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.delete').on('click', function () {
        alert('are you sure that ');

        var pet_id = $(this).parent().attr('data-pet_id');
        $.ajax({
            url: `/delete/pet`,
            method: `POST`,
            data:
                {
                    "pet_id": pet_id


                }, success: function () {
                window.location.reload();
            }

    })

    })
});

